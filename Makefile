SHELL = /usr/bin/env bash
BIN2O = $(PS2SDK)/bin/bin2o

EE_BIN = patloader.elf
EE_BIN_PKD = patloader_packed.elf
EE_OBJS = patloader.o

# Based heavily on loader.elf from uLE
# Took out the manual stack and loading stuff though,
#  since I have no idea how to configure it myself

EE_INCS := $(EE_INCS) -I$(PS2SDK)/sbv/include

EE_CFLAGS  := -mips3 -ffreestanding -fno-builtin -G0 -fdata-sections -ffunction-sections -Wl,--gc-sections \
	-fshort-double -mlong64 -mhard-float -mno-abicalls -O2 -EL -Wall \
	$(EE_INCS) $(EE_CFLAGS)

EE_ASFLAGS := -EL -G0 $(EE_ASFLAGS)

EE_LDFLAGS := $(EE_LDFLAGS) -L$(PS2SDK)/sbv/lib

EE_LIBS = -lpatches -lhdd -lpoweroff -lfileXio
ifeq ($(DEBUG),1)
	EE_LIBS += -ldebug
	EE_CFLAGS += -DDEBUG
endif

IRXFILES = \
	$(PS2SDK)/iop/irx/iomanX.irx \
	$(PS2SDK)/iop/irx/fileXio.irx \
	$(PS2SDK)/iop/irx/poweroff.irx \
	$(PS2SDK)/iop/irx/ps2dev9.irx \
	$(PS2SDK)/iop/irx/ps2atad.irx \
	$(PS2SDK)/iop/irx/ps2hdd.irx \
	$(PS2SDK)/iop/irx/ps2fs.irx

EE_OBJS += $(addprefix ee/,$(addsuffix _irx.o,$(basename $(notdir $(IRXFILES)))))


all: builtins
	$(MAKE) $(EE_BIN)
ifneq ($(DEBUG),1)
	@echo "Stripping..."
	@ee-strip $(EE_BIN)
	@echo "Compressing..."
	@ps2-packer $(EE_BIN) $(EE_BIN_PKD) > /dev/null
endif


builtins:
	@mkdir -p ee
	@for file in $(IRXFILES); do \
		basefile=$${file/*\//}; \
		basefile=$${basefile/\.*/}; \
		echo "Embedding IRX file $$basefile"; \
		$(BIN2O) $$file ee/$${basefile}_irx.o _binary_$${basefile}_irx; \
	done;
clean:
	@echo "Cleaning..."
	@rm -rf ee
	@rm -f *.o *.elf

rebuild: clean all

maketest: rebuild
	@echo "Moving to host..."
	@mv $(EE_BIN) /media/sf_Documents/patloader.elf

include $(PS2SDK)/samples/Makefile.pref
include $(PS2SDK)/samples/Makefile.eeglobal
