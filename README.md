Patloader is a small PS2 program that runs BOOT.ELF on the specified
hard-drive partition. It is designed to be embedded into a partition
header, where it will auto-detect which partition it is on when run.
If it fails for any reason, it attempts to restart the PS2 to the OSDSYS.

## How to use

Simply embed patloader.elf into the target partition's header. A
common way to do this is the _modify_header_ command in AKuHAK's version
of [HDL Dump](https://bitbucket.org/AKuHAK/hdl-dump/). Make sure the system.cnf
that is injected uses `BOOT2 = PATINFO`.

Once embedded, simply place an ELF on the same partition named _BOOT.ELF_.

## Technical Details

Builds with the latest (as of this time) PS2SDK.

This ELF uses argv[0] to detect which partition to work with. It will
technically work from any location, as long as you can set argv[0].

# Credits

Code in uLaunchELF's loader.elf and Open PS2 Loader were used as a reference.
Especially loader.elf, for how to load an ELF to memory and execute it.  
(Related: Why does the documentation for the PS2SDK suck so bad? Or rather,
not exist. -.- )

All modules used are directly from the PS2SDK.
