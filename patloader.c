#include <kernel.h>
#include <sifrpc.h>		// SifInitRpc
#include <loadfile.h>		// SifExecModuleBuffer
#include <sbv_patches.h>	// sbv_patch_enable_lmb
#include <iopheap.h>		// SifExitIopHeap
#include <iopcontrol.h>		// SifIopReset
#include <fileXio_rpc.h>
#include <libhdd.h>
#include <fcntl.h>		// FIO_MT_RDONLY
#include <string.h>		// mset

//#define DEBUG

#define MOUNT_PATH	"pfs0:"
#define BOOT_PATH	MOUNT_PATH "/BOOT.ELF"
#define ELF_MAGIC	0x464C457F
#define ELF_PT_LOAD	1

#ifdef DEBUG
	#include <debug.h>	// scr_printf
	#define dbginit_scr() init_scr()
	#define dbgprintf(args...) scr_printf(args)
	#warning Debug on!
#else
	#define dbginit_scr() do { } while(0)
	#define dbgprintf(args...) do { } while(0)
#endif


/* Embeded IRX files */
	extern unsigned char _binary_iomanX_irx_start[];
	extern unsigned int _binary_iomanX_irx_size;

	extern unsigned char _binary_fileXio_irx_start[];
	extern unsigned int _binary_fileXio_irx_size;

	extern unsigned char _binary_poweroff_irx_start[];
	extern unsigned int _binary_poweroff_irx_size;

	extern unsigned char _binary_ps2dev9_irx_start[];
	extern unsigned int _binary_ps2dev9_irx_size;

	extern unsigned char _binary_ps2atad_irx_start[];
	extern unsigned int _binary_ps2atad_irx_size;

	extern unsigned char _binary_ps2hdd_irx_start[];
	extern unsigned int _binary_ps2hdd_irx_size;

	extern unsigned char _binary_ps2fs_irx_start[];
	extern unsigned int _binary_ps2fs_irx_size;

/* Structures */
	typedef struct{
		u32	magic;
		u8	ident[12];
		u16	type;
		u16	machine;
		u32	version;
		u32	entry;
		u32	phoff;
		u32	shoff;
		u32	flags;
		u16	ehsize;
		u16	phentsize;
		u16	phnum;
		u16	shentsize;
		u16	shnum;
		u16	shstrndx;
	} elf_header_t;
	typedef struct{
		u32	type;
		u32	offset;
		void	*vaddr;
		u32	paddr;
		u32	filesz;
		u32	memsz;
		u32	flags;
		u32	align;
	} elf_pheader_t;

static int tLoadElf(char *filename, t_ExecData *elfdata){
	elf_header_t* eh = (elf_header_t*)0x01800000;
	elf_pheader_t* eph;

	int fd;
	if((fd = fileXioOpen(filename, O_RDONLY, FIO_S_IRUSR | FIO_S_IRGRP | FIO_S_IROTH)) < 0){
		dbgprintf("Failed in fileXioOpen %s\n", filename);
		goto error;
	}
	dbgprintf("Opened file %s\n", filename);

	int size = fileXioLseek(fd, 0, SEEK_END);
	dbgprintf("File size = %i\n", size);
	if(size <= 0){
		dbgprintf("Failed in fileXioLseek\n");
		fileXioClose(fd);
		goto error;
	}

	fileXioLseek(fd, 0, SEEK_SET);
	fileXioRead(fd, eh, sizeof(elf_header_t));
	dbgprintf("Read elf header from file\n");

	if(eh->magic != ELF_MAGIC){
		dbgprintf("Not a recognised ELF.\n");
		fileXioClose(fd);
		goto error;
	}

	fileXioLseek(fd, eh->phoff, SEEK_SET);
	eph = (elf_pheader_t*)(eh + eh->phoff);
	size = fileXioRead(fd, eph, eh->phnum * eh->phentsize);
	dbgprintf("Read %i bytes of program header(s) from file\n", size);

	for(size = 0; size < eh->phnum; ++size){
		if(eph[size].type == ELF_PT_LOAD){
			fileXioLseek(fd, eph[size].offset, SEEK_SET);
			fileXioRead(fd, eph[size].vaddr, eph[size].filesz);
			dbgprintf("Read %i bytes to %p\n", eph[size].filesz, eph[size].vaddr);
			if(eph[size].memsz > eph[size].filesz){
				memset(eph[size].vaddr + eph[size].filesz, 0, eph[size].memsz - eph[size].filesz);
			}
		}
	}

	fileXioClose(fd);

	dbgprintf("Entry point = %x\n", eh->entry);
	elfdata->epc = eh->entry;
	return 0;
error:
	elfdata->epc = 0;
	return -1;
}

int LoadModules(){
	int ret;
	// Only allow 1 mount
	static const char hddarg[] = "-o" "\0" "1" "\0" "-n" "\0" "5";
	// Only 1 mount, but also only 1 open file
	static const char pfsarg[] = "-m" "\0" "1" "\0" "-o" "\0" "1" "\0" "-n" "\0" "10";

	dbgprintf("poweroff.irx %i bytes\n", _binary_poweroff_irx_size);
	SifExecModuleBuffer(&_binary_poweroff_irx_start, _binary_poweroff_irx_size, 0, NULL, &ret);
	if(ret < 0){goto error;}

	dbgprintf("iomanx.irx %i bytes\n", _binary_iomanX_irx_size);
	SifExecModuleBuffer(&_binary_iomanX_irx_start, _binary_iomanX_irx_size, 0, NULL, &ret);
	if(ret < 0){goto error;}

	dbgprintf("filexio.irx %i bytes\n", _binary_fileXio_irx_size);
	SifExecModuleBuffer(&_binary_fileXio_irx_start, _binary_fileXio_irx_size, 0, NULL, &ret);
	if(ret < 0 || fileXioInit() < 0){goto error;}

	dbgprintf("ps2dev9.irx %i bytes\n", _binary_ps2dev9_irx_size);
	SifExecModuleBuffer(&_binary_ps2dev9_irx_start, _binary_ps2dev9_irx_size, 0, NULL, &ret);
	if(ret < 0){goto error;}

	dbgprintf("ps2atad.irx %i bytes\n", _binary_ps2atad_irx_size);
	SifExecModuleBuffer(&_binary_ps2atad_irx_start, _binary_ps2atad_irx_size, 0, NULL, &ret);
	if(ret < 0){goto error;}

	dbgprintf("ps2hdd.irx %i bytes\n", _binary_ps2hdd_irx_size);
	SifExecModuleBuffer(&_binary_ps2hdd_irx_start, _binary_ps2hdd_irx_size, sizeof(hddarg), hddarg, &ret);
	if(ret < 0){goto error;}

	dbgprintf("ps2fs.irx %i bytes\n", _binary_ps2fs_irx_size);
	SifExecModuleBuffer(&_binary_ps2fs_irx_start, _binary_ps2fs_irx_size, sizeof(pfsarg), pfsarg, &ret);
	if(ret < 0){goto error;}

	dbgprintf("LoadModules() complete!\n");
	return 0;
error:
	dbgprintf("ERROR: %s: failed to load module: %i\n", __FUNCTION__, ret);
	return -1;
}

/* HDDOSDSYS boots embedded partition ELFs like this:
 * argv[0] = "hdd0:PP.TEST:PATINFO"
 *           HDD device name:partition ID:contents of BOOT2
 * However, this seems to be the "normal" argument set:
 * argv[0] = Boot path ("pfs:/TEST.ELF")
 * argv[1] = HDD device name:partition ID:contents of BOOT2
 */
int main(int argc, char* argv[]){
  	SifInitRpc(0);
	while(!SifIopReset(NULL, 0)){}
	// So I tried to put the string manip. here, but causes a crash in release mode. Go figure.
	while(!SifIopSync()){}
	SifInitRpc(0);
	sbv_patch_enable_lmb();

	dbginit_scr();
	dbgprintf("HDD Boot\n");

	char *exec_argv[3];

	// A partition name can be at most 32 characters (including NULL).
	// The +5 is for the "hdd0:" string.
	char partition[5+32];

	// Lobotomizing argc after the first check. Only argv[0] is used, so might as well.
	if(argc < 1 || strncmp(argv[0], "hdd0:", 5) != 0 || (exec_argv[0] = strchr(argv[0]+5, ':')) == NULL || (argc = exec_argv[0] - argv[0]) >= sizeof(partition)){
		dbgprintf("No arguments or failed to parse, aborting!\n");
		goto error;
	}
	strncpy(partition, argv[0], argc);
	partition[argc] = 0x00;	//Manual NULL terminate

	dbgprintf("HDD Partition = %s\n", partition);

	// If hddCheckFormatted() is 0, than hddCheckPresent() must also be 0.
	if(LoadModules() < 0 || hddCheckFormatted() < 0){
		dbgprintf("LoadModules() or hddCheckFormatted() fail\n");
		goto error;
	}

	if(fileXioMount(MOUNT_PATH, partition, FIO_MT_RDONLY) < 0){
		// Some error occurred, could be because pfs# is already used.
		// So unmount pfs# and try again.
		fileXioUmount(MOUNT_PATH);
		if(fileXioMount(MOUNT_PATH, partition, FIO_MT_RDONLY) < 0){
			dbgprintf("HDD mount failed\n");
			goto error;
		}
	}

	t_ExecData elf;
	argc = tLoadElf(BOOT_PATH, &elf);
	fileXioUmount(MOUNT_PATH);
	if(argc < 0 || elf.epc == 0){
		dbgprintf("Could not load file " BOOT_PATH "\n");
		goto error;
	}

	exec_argv[0] = BOOT_PATH;
	exec_argv[1] = argv[0];
	exec_argv[2] = NULL;

	// Exit services (partially copied from OPL)
	fileXioExit();
	SifLoadFileExit();
	SifExitIopHeap();
	SifExitRpc();

	FlushCache(0);	//Write back contents of datacache and invalidate
	FlushCache(2);	//Invalidate contents of instruction cache
	ExecPS2((void*)elf.epc, (void*)elf.gp, 2, exec_argv);
	return 0;
error:
	// If we fail for any reason, reset back to the OSDSYS
	dbgprintf("Return to Browser...\n");
	FlushCache(0);
	FlushCache(2);
	__asm__ __volatile__("li $3, 0x04;syscall;nop;");
	return -1;
}
